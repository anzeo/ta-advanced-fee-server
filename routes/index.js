var data = require("../data/data"),
    jsonMapper = require("../utils/JsonMapper");

exports.index = function(req, res){
    res.send(jsonMapper.mapMenuItem(data.menuRoot));
}