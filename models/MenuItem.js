function MenuItem(title, parent){
    this.title = title;
    this.children = [];
    if(parent) {
        parent.children.push(this);
        var parentPath = parent.path || "items/";
        this.path = parentPath + (title.toLowerCase().replace(/\s+/g, '')) + "/";
        this.parentPath = parentPath;
    } else {
        this.path = "";
    }
}

module.exports = exports = MenuItem;