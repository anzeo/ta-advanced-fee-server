var nextOrderId = 1;

function Order(items){
    this.id = nextOrderId++;
    this.items = items;
    this.date = new Date();
}

module.exports = exports = Order;