var BASE_URL = "http://localhost:1337/";

/**
 * maps an item in the menu
 * @param item
 * @returns Object a JSON representation of the item
 */
exports.mapMenuItem = function(item){
    if(!item)
        return undefined;

    var json = {
        _links: {
            self: {
                href: BASE_URL + item.path
            }
        },
        title: item.title,
        _embedded: {
            items: []
        }
    },
        child;

    if(item.parentPath){
        json._links.parent = {
            href: BASE_URL + item.parentPath
        }
    }

    for(var i = 0; i < item.children.length; i++){
        child = item.children[i];
        var jsonChild = {
            title: child.title,
            _links: {}
        };

        jsonChild._links[child.children.length > 0 ? "children" : "self"] = {
            href: BASE_URL + child.path
        }

        json._embedded.items.push(jsonChild)
    }

    return json;
}

/**
 * Maps an underlying item
 * @param parent the parent of the underlying item
 * @returns Object
 */
exports.mapUnderlyingItem = function(parent){
    if(!parent)
        return undefined;

    var json = parent.underlyingItem;

    json._actions = {};
    return json;
}

/**
 * Maps a list of orders
 * @param orders
 * @returns {{_embedded: {items: Array}}}
 */
exports.mapOrders = function(orders){
    var mappedOrders = [];
    for(var i = 0; i < orders.length; i++){
        mappedOrders.push(mapOrder(orders[i]));
    }

    return {
        _embedded: {
            items: mappedOrders
        }
    };
}

/**
 * Maps an order
 * @param order
 * @returns {{id: (*|module.id|string|Order.id|id), date: number}}
 */
function mapOrder(order){

    var mappedOrder = {
        id: order.id,
        date: order.date.getTime()
    };

    var orderedItems = [];
    // we need items, but we need the menuitems above them as well
    for(var j =  0; j < order.items.length; j++){
       orderedItems.push(mapOrderedItem(order.items[j]))
    }

    mappedOrder._embedded = {
        items: orderedItems
    }

    return mappedOrder;
}

/**
 * Maps ordered item to a menu item
 * @param orderedItem
 * @returns {{title: (*|string|MenuItem.title), _links: {self: {href: (path|*|exports.data.path|req.path|file.path|opts.path|MenuItem.path|Route.path|View.path|string|SendStream.path|Function|ReadStream.path|WriteStream.path)}}}}
 */
function mapOrderedItem(orderedItem){;
    return {
        title: orderedItem.name,
        _links: {
            self: {
                href: BASE_URL + orderedItem.path
            }
        }
    };
}